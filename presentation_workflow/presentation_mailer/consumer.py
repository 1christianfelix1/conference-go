import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
      'Your presentation has been accepted',
      f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
      'admin@conference.go',
      [content['presenter_email']],
      fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
      'Your presentation has been rejected',
      f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been rejected",
      'admin@conference.go',
      [content['presenter_email']],
      fail_silently=False,
    )


# Create a main method to run

    # Set the hostname that we'll connect to
parameters = pika.ConnectionParameters(host='rabbitmq')

# Create a connection to RabbitMQ
connection = pika.BlockingConnection(parameters)

# Open a channel to RabbitMQ
channel = connection.channel()

# Create a queue if it does not exist
channel.queue_declare(queue='presentation_approvals')
# Configure the consumer to call the process_message function
# when a message arrives
channel.basic_consume(
    queue='presentation_approvals',
    on_message_callback=process_approval,
    auto_ack=True,
)

# Create a queue if it does not exist
channel.queue_declare(queue='presentation_rejections')
# Configure the consumer to call the process_message function
# when a message arrives
channel.basic_consume(
    queue='presentation_rejections',
    on_message_callback=process_rejection,
    auto_ack=True,
)
# Print a status
print(' [*] Waiting for messages. To exit press CTRL+C')

# Tell RabbitMQ that you're ready to receive messages
channel.start_consuming()
