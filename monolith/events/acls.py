from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": "PEXELS_API_KEY"}
    print("fasdfdsafjksdjf", type(state))
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    print("\nhere\n")
    response = requests.get(url, headers=headers, params=params)
    print("\nhere11\n")
    print(response.content)
    content = json.loads(response.content)
    print("\nhere22\n")

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    url_geo = "http://api.openweathermap.org/geo/1.0/direct"
    params_geo = {
        "q": city + "," + state + "," + "USA",
        "limit": "1",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url_geo, headers=headers, params=params_geo)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url_curr_weather = "https://api.openweathermap.org/data/2.5/weather"

    response = requests.get(
        url_curr_weather,
        headers=headers,
        params={
            "lat": lat,
            "lon": lon,
            "units": "imperial",
            "appid": OPEN_WEATHER_API_KEY,
        },
    )
    content = json.loads(response.content)
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]

    try:
        return {"temp": temp, "description": description}
    except:
        return None
