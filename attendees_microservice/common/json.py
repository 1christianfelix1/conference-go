from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet
from pprint import pprint

# this executes when passed in a query set. A queryset class is created by accessing all of instances of a model
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            # convert a dictionary to a list, possibly variable set to objects.all() would execute this encoder
            print("\nQuerySetEncoder: ", list(o))
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            print("DateEncoder: ", o)
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # encoders decalred here for when an ecoder doesn't have an encoders dictionary.
    # This avoids conflict with the for loop in self.encoders
    encoders = {}

    def default(self, o):
        print("-----------------------------------")
        print("expected class: ", self.model)
        print("type of object passed in: ", type(o), "\n")
        # print("\n")
        if isinstance(o, self.model):
            print("ModelEncoder: ", o)
            d = {}
            if hasattr(o, "get_api_url"):
                print("get_api_url attribute found. adding href...")
                d["href"] = o.get_api_url()
            for property in self.properties:
                #  getattr() takes an object or class and a name of a property and returns the value of that property
                value = getattr(o, property)
                print(
                    "property_name: ",
                    property,
                    " --> ",
                    "property_value: ",
                    value,
                    " --> ",
                    "value_type: ",
                    type(value),
                )
                if property in self.encoders:
                    encoder = self.encoders[property]
                    print("\nEncoder Found: ", encoder)
                    value = encoder.default(value)
                    print("Encoder return: ", value)
                d[property] = value
            d.update(self.get_extra_data(o))
            # pprint(d)
            return d
        else:
            test = super().default(o)
            print("converted: ", test, type(test))
            return test

    def get_extra_data(self, o):
        return {}
